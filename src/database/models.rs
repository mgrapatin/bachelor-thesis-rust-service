use serde::{Deserialize, Serialize};
use diesel::prelude::*;
use super::schema::notes;
use super::schema::notes::dsl::notes as notes_dsl;

#[derive(Debug, Deserialize, Serialize, Queryable, Insertable)]
#[table_name = "notes"]
pub struct Note {

    /// Die ID der Notiz
    id: i32,
    /// Steuert, ob die Notiz erweiterbar ist, oder ob sie statisch/starr ist.
    appendable: Option<bool>,
    /// Das Erzeugungsdatum der Notiz (nicht der Details!).
    creation_date: Option<String>,
    /// Kundennummer
    customer_id: Option<String>,
    /// Liste der Details
    details: Option<String>,
    /// Liste der References
    reference: Option<String>,

}


impl Note {

    pub fn list_all(sqlite_connection: &SqliteConnection) -> Vec<Self> {
        notes_dsl.load::<Note>(sqlite_connection).expect("Die Notes konnten nicht geladen werden!")
    }

    pub fn find_note_by_id(id: &i32, sqlite_connection: &SqliteConnection) -> Option<Self> {
        notes_dsl.find(id).get_result::<Note>(sqlite_connection).ok()
    }

    pub fn create_note(note: Self, sqlite_connection: &SqliteConnection) -> Option<Self> {
        diesel::insert_into(notes_dsl)
            .values(&note)
            .execute(sqlite_connection)
            .expect("Die Note konnte nicht gespeichert werden!");

        Self::find_note_by_id(&note.id, sqlite_connection)
    }
}
table! {
    notes (id) {
        id -> Integer,
        appendable -> Nullable<Bool>,
        creation_date -> Nullable<Timestamp>,
        customer_id -> Nullable<Text>,
        details -> Nullable<Text>,
        reference -> Nullable<Text>,
    }
}

use diesel_migrations::embed_migrations;
use diesel::sqlite::SqliteConnection;
use r2d2_diesel::ConnectionManager;
use r2d2::Pool;

embed_migrations!("migrations");

pub type SqliteDatabaseConnectionPool = Pool<ConnectionManager<SqliteConnection>>;

pub fn establish_connection() -> SqliteDatabaseConnectionPool {
        let database_url = "file::memory:?cache=shared".to_string();
        let connection_manager = ConnectionManager::<SqliteConnection>::new(&database_url);

        r2d2::Pool::builder().build(connection_manager).expect("Der Datenbank Pool konnte nicht erstellt werden")
}
#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_migrations;
extern crate serde_json;
extern crate r2d2_diesel;

mod dbpool;
mod services;
mod router;
mod database;

diesel_migrations::embed_migrations!();

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    use actix_web::{App, HttpServer, web::JsonConfig};

    let sqlite_connection_pool = dbpool::establish_connection();
    let connection_for_migration = sqlite_connection_pool.clone();
    let _ = embedded_migrations::run(&*connection_for_migration.get().unwrap());

    HttpServer::new(move || {
        App::new()
            .data(sqlite_connection_pool.clone())
            .data(JsonConfig::default().limit(4096))
            .configure(router::initialise_routes)
    })
        .bind("0.0.0.0:8080")?
        .workers(8)
        .run()
        .await
}

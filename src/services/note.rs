use actix_web::{web, HttpResponse};
use crate::dbpool::SqliteDatabaseConnectionPool;
use crate::database::models::Note;

pub fn create_note(note: web::Json<Note>, sqlite_connection_pool: web::Data<SqliteDatabaseConnectionPool>) -> HttpResponse {
    let sqlite_connection = sqlite_connection_pool.get().unwrap();

    match Note::create_note(note.into_inner(), &sqlite_connection) {
        Some(note) => {HttpResponse::Ok().json(note)}
        None => HttpResponse::InternalServerError().json("Die Notiz konnte nicht angelegt werden.")
    }
}

pub fn list_all_notes(sqlite_connection_pool: web::Data<SqliteDatabaseConnectionPool>) -> HttpResponse {
    let sqlite_connection = sqlite_connection_pool.get().unwrap();
    HttpResponse::Ok().json(Note::list_all(&sqlite_connection))
}

pub fn find_note_by_id(id: web::Path<String>, sqlite_connection_pool: web::Data<SqliteDatabaseConnectionPool>) -> HttpResponse {
    let sqlite_connection = sqlite_connection_pool.get().unwrap();
    match Note::find_note_by_id(&id.parse::<i32>().unwrap(), &sqlite_connection) {
        Some(note) => {HttpResponse::Ok().json(note)}
        None => HttpResponse::NotFound().json("Unter dieser ID ist keine Notiz vorhanden")
    }
}



use actix_web::web;

pub fn initialise_routes(configuration: &mut web::ServiceConfig) {

    configuration.service(
        web::resource("/notes")
            .route(web::post().to(crate::services::note::create_note))
            .route(web::get().to(crate::services::note::list_all_notes))
    ).service(
        web::scope("/note")
            .route("/{id}", web::get().to(crate::services::note::find_note_by_id))
    ).service(
        web::resource("/loaderio-98e0ad2e72a6229521e6c2567cc12f17/").route(web::get().to(crate::services::verification::verify))
    );
}
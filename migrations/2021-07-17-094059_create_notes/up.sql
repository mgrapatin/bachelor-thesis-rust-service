-- Your SQL goes here
CREATE TABLE IF NOT EXISTS notes (
    id INTEGER NOT NULL PRIMARY KEY,
    appendable BOOLEAN,
    creation_date DATETIME,
    customer_id TEXT,
    details TEXT,
    reference TEXT
);